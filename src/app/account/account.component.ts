import { Component, OnInit } from '@angular/core';
import { User } from '../Model/User/User.model';
import { Subscription } from 'rxjs';
import { UserService } from '../services/user.service';
import { UserDetail } from '../Model/User/UserDetails.model';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {

  private user: User = new User; 
  private userSub: Subscription; 

  private passwordChange  = {oldPassword:'', newPassString:'', newPassStringRep: ''};

  userDetails: UserDetail = new UserDetail();

  constructor(private userService: UserService) { 
  }

  ngOnInit() {
    console.log("User Details ");
    this.userSub = this.userService.getUser().subscribe((user)=>{this.user = user;
      this.userDetails = user.userDetails;
      console.log("User Details ");
      console.log( this.user.userDetails );
    });
  }

  ngOnDestroy(){
    this.userSub.unsubscribe();
  }

  updateUserDetails(){
    this.userService.updateUserDetails( this.userDetails );
  }

  changePassword(){
    this.userService.changePassword( this.passwordChange );
    this.passwordChange.newPassString = '';
    this.passwordChange.newPassStringRep = ''; 
    this.passwordChange.oldPassword = '';
  }

  deleteAcount(){
    this.userService.deleteAcount();
  }
}
