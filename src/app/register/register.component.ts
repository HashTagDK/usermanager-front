import { Component, OnInit } from '@angular/core';
import { User } from '../Model/User/User.model';
import { AppService } from '../app.service';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  constructor(private userService: UserService) { }

  incorrectCredencialsMessage: string = "";

  user: User;
  repeatedPassword: string;
  registered: boolean;

  ngOnInit() {
    this.user = new User();
    this.registered = false;
  }

  register() {
    if (this.user.password.length < 3) {
      this.incorrectCredencialsMessage = "Password length have to be longer than 3 char!"
    } else if (!this.user.password.match(this.repeatedPassword)) {
      this.incorrectCredencialsMessage = "Password dont match!";
    } else {
      this.incorrectCredencialsMessage = "";
      this.userService.register(this.user);
    }
  }
}
