import { Authority } from "./Authority.model";
import { UserDetail } from "./UserDetails.model";

export class User{
    username: string; 
    password: string;
    email: string;
    enabled: boolean;
    authorities: Authority[]; 
    userDetails: UserDetail;
}