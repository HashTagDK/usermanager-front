export class Auth{
    username: string; 
    admin: boolean;
    user: boolean; 
    authenticated: boolean;

    constructor(username: string, admin: boolean, user:boolean, authenticated: boolean){
        this.username = username;
        this.admin = admin;
        this.user = user; 
        this.authenticated = authenticated;
    }
}