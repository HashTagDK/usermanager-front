export class UserDetail{
    id: number;
    name: string;
    surname: string;
    address: string;
    postCode: string;
    city: string;
    telephonNumber: string;
    emailAdress: string;
    birhtDate: Date;
    singUpDate: Date;
    lastModificationDate: Date;
    complete: boolean;

    constructor(){}
}