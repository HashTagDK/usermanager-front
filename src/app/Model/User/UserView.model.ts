export class UserView{
    username: string;
    enabled: boolean;
    name: string;
    surname: string;
    singUpDate: Date;
    lastModificationDate: Date;
    complete: boolean;

    constructor(){}
}