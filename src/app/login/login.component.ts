import { Component, OnInit } from '@angular/core';
import { Credentials } from '../Model/Credentials.model';
import { AppService } from '../app.service';
import { Router } from '@angular/router';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private userService: UserService, private router: Router) { }

  ngOnInit() {
  }

  credentials: Credentials = new Credentials();

  login() {
    console.log("login():")
    console.log( this.credentials );
    this.userService.authenticate(this.credentials, () => {
        this.router.navigateByUrl('/');
    });
    return false;
  }

}
