import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { Auth } from '../Model/User/Auth.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {

  private auth: Auth;
  private authSub: Subscription; 

  constructor(private userService: UserService) { 
    
  }

  ngOnInit() {
    this.auth =  new Auth(null, false, false, false );
    this.authSub = this.userService.getAuth().subscribe((auth)=> this.auth = auth);
  }

  ngOnDestroy(){
    this.authSub.unsubscribe();
  }

  logout(){
    this.userService.logout();
  }
}
