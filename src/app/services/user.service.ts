import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import 'rxjs/Rx';
import { User } from '../Model/User/User.model';
import { Subject } from 'rxjs/Rx';
import { Observable } from 'rxjs';
import { MessageInfo } from '../Model/MessageInfo.model';
import { MessageService } from './message.service';
import { Auth } from '../Model/User/Auth.model';
import { Authority } from '../Model/User/Authority.model';
import { UserDetail } from '../Model/User/UserDetails.model';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class UserService {
    private user: User = new User();
    private userSubject = new BehaviorSubject<User>(new User());
    private authSubejct = new Subject<Auth>();

    constructor(private http: HttpClient, private router: Router, private messageService: MessageService) {
        this.setAuth(new Auth(null, false, false, false));
        const user: User = new User();
        user.userDetails = new UserDetail();

        this.setUser(user);
    }

    setUser(user: User) {
        this.user = user;
        this.userSubject.next(user);
    }
    clearUser() {
        this.userSubject.next(null);
    }
    getUser(): Observable<User> {
        return this.userSubject.asObservable();
    }


    setAuth(auth: Auth) {
        this.authSubejct.next(auth);
    }
    clearAuth() {
        this.authSubejct.next();
    }
    getAuth(): Observable<Auth> {
        return this.authSubejct.asObservable();
    }

    setAuthModel(user: User): Auth {
        const auth: Auth = new Auth(user.username, false, false, true);
        user.authorities.forEach((a) => {
            if (a.authority == "ROLE_USER") {
                auth.user = true;
            } else if (a.authority == "ROLE_ADMIN") {
                auth.admin = true;
            }
        })
        console.log(auth);
        return auth;
    }

    authenticate(credentials, callback) {
        const headers = new HttpHeaders(credentials ? {
            authorization: 'Basic ' + btoa(credentials.username + ':' + credentials.password)
        } : {});
        this.http.get<User>('/api/user', { headers: headers })
            .subscribe(
                (user: User) => {

                    this.setAuth(this.setAuthModel(user));
                    this.setUser(user);
                    this.messageService.sendMessage(new MessageInfo("primary", "Authenticated!"));
                    return callback && callback();
                },
                (error) => {
                    this.setAuth(new Auth(null, false, false, false));
                    this.setUser(null);
                    this.messageService.sendMessage(new MessageInfo("danger", "Bad credencials!"));
                }
            )
    }

    logout() {
        this.http.post('/api/logout', {}).finally(() => {
            this.setAuth(new Auth(null, false, false, false));
            this.setUser( new User() );
            this.router.navigateByUrl('/login');
            this.messageService.sendMessage(new MessageInfo("info", "Logout!"));
        }).subscribe();
    }

    register(user: User) {
        this.http.post('/api/addUser', user)
            .subscribe((response: Response) => {
                this.messageService.sendMessage(new MessageInfo("success", "Acount registered!"));
            });
    }

    updateUserDetails(userDetails: UserDetail) {
        this.http.put('/api/user/userDetails', userDetails).subscribe((userDetails: UserDetail) => {
            this.user.userDetails = userDetails;
            this.setUser(this.user);
            this.messageService.sendMessage(new MessageInfo('success', "user detail updated!"));
        },
            (err) => {
                this.messageService.sendMessage(new MessageInfo('warning', "sth went wrong!"));
            }
        )
    }

    changePassword(passwordChange: any) {

        if (passwordChange.newPassString.length < 3) {
            this.messageService.sendMessage(new MessageInfo('warning', "Password length have to be longer than 3 char!!"));
        } else if (!passwordChange.newPassString.match(passwordChange.newPassStringRep)) {
            this.messageService.sendMessage(new MessageInfo('warning', "Password dont match!"));
        } else {
            this.http.put('/api/user', passwordChange).subscribe((passUpdated: boolean) => {
                if (passUpdated) {
                    this.messageService.sendMessage(new MessageInfo('success', "password updated!"));
                } else {
                    this.messageService.sendMessage(new MessageInfo('danger', "old password dont match"));
                }

            },
                (err) => {
                    this.messageService.sendMessage(new MessageInfo('warning', "sth went wrong!"));
                }
            )
        }
    }

    deleteAcount() {
        this.http.delete('/api/user').subscribe((deleted: boolean) => {
            if (deleted) {
                this.messageService.sendMessage(new MessageInfo('success', "accout deleted!"));
                this.setAuth( new Auth('', false, false, false ));
                this.router.navigateByUrl('/');
            } else {
                this.messageService.sendMessage(new MessageInfo('waringn', "sth went wrong!"));
            }
        },
            (err) => {

            }
        )
    }
}