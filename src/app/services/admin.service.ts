import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import 'rxjs/Rx';
import { User } from '../Model/User/User.model';
import { Subject, BehaviorSubject } from 'rxjs/Rx';
import { Observable } from 'rxjs';
import { MessageInfo } from '../Model/MessageInfo.model';
import { UserView } from '../Model/User/UserView.model';
import { UserService } from './user.service';
import { MessageService } from './message.service';

@Injectable()
export class AdminService {
    private userViewArray: UserView[];
    private userViewSub = new BehaviorSubject<UserView[]>([]);
    constructor(private http: HttpClient, private messageService: MessageService) { }

    setUserViewArray( userViewArray: UserView[] ){
        this.userViewArray = userViewArray;
        this.userViewSub.next( userViewArray );
    }
    getUserViewArrayObs(): Observable<UserView[]>{
        return this.userViewSub.asObservable();
    }

    getUserView(){
        this.http.get<UserView[]>('/api/admin/user').subscribe(
            (userViewA: UserView[])=>{
                this.setUserViewArray( userViewA );
            },
            (error)=>{

            }
        )
    }

    deleteUser(userView: UserView){
        this.http.delete<boolean>("/api/admin/user/" + userView.username).subscribe(
            ( deleted: boolean )=>{
                if( deleted ){
                    this.messageService.sendMessage(new MessageInfo('success', "User: "+userView.username+" deleted!"));
                    console.log(this.userViewArray.indexOf(userView));
                    this.userViewArray.splice(this.userViewArray.indexOf(userView), 1);

                    this.userViewSub.next( this.userViewArray );
                }else{
                    this.messageService.sendMessage(new MessageInfo('warning', "Error ocured User: "+userView.username+" deleted!"));
                }
            }
        )
    }

    disable(uV:UserView, disable: boolean){
        uV.enabled = disable;
        this.http.put<UserView>('/api/admin/user/disable', uV).subscribe(
            (uVResponse: UserView)=>{
                if( uVResponse.enabled ){
                    this.messageService.sendMessage(new MessageInfo('primary', "User: "+uV.username+" enabled!"));
                }else{
                    this.messageService.sendMessage(new MessageInfo('warning', "User: "+uV.username+" disabled!"));
                } 
                this.userViewArray[this.userViewArray.indexOf(uV)] = uVResponse;
                this.userViewSub.next( this.userViewArray );
            }, 
            (error)=>{
                this.messageService.sendMessage(new MessageInfo('danger', "sth went wrong!"));
            }
        )
    }
}