import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import 'rxjs/Rx';
import { User } from '../Model/User/User.model';
import { Subject } from 'rxjs/Rx';
import { Observable } from 'rxjs';
import { MessageInfo } from '../Model/MessageInfo.model';

@Injectable()
export class MessageService {
  private messageSubject = new Subject<MessageInfo>();

  constructor(private http: HttpClient) {}

  sendMessage(message: MessageInfo){
      this.messageSubject.next( message );
  }
  clearMessage(){
      this.messageSubject.next();
  }
  getMessage(): Observable<MessageInfo>{
      return this.messageSubject.asObservable();
  }
}