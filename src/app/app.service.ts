import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import 'rxjs/Rx';
import { User } from './Model/User/User.model';
import { Subject } from 'rxjs/Rx';
import { Observable } from 'rxjs';
import { MessageService } from './services/message.service';
import { MessageInfo } from './Model/MessageInfo.model';

@Injectable()
export class AppService {

  user: User;
  authenticated = false;

  constructor(private http: HttpClient, private router: Router, private messageService: MessageService) {

  }
}