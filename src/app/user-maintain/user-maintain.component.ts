import { Component, OnInit } from '@angular/core';
import { AdminService } from '../services/admin.service';
import { UserView } from '../Model/User/UserView.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-user-maintain',
  templateUrl: './user-maintain.component.html',
  styleUrls: ['./user-maintain.component.css']
})
export class UserMaintainComponent implements OnInit {
  private userViewArray: UserView[];
  private userViewObs: Subscription;

  constructor(private adminService: AdminService) { }

  ngOnInit() {
    this.adminService.getUserView();
    this.userViewObs = this.adminService.getUserViewArrayObs().subscribe((uVA) => {
      this.userViewArray = uVA;
    })
  }

  deleteUser(uV: UserView){
    this.adminService.deleteUser( uV );
  }

  disable(uV:UserView, disbale: boolean){
    this.adminService.disable( uV,  disbale);
  }

}
