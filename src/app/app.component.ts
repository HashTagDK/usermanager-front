import { Component } from '@angular/core';
import { AppService } from './app.service';
import { UserService } from './services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  constructor(private userService: UserService, private router: Router){}

  ngOnInit(){
    this.userService.authenticate(null, () => {
      this.router.navigateByUrl('/')});
  }
}
