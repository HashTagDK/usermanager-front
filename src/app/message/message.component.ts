import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { MessageService } from '../services/message.service';
import { MessageInfo } from '../Model/MessageInfo.model';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
export class MessageComponent implements OnInit {
  messageInfo: MessageInfo;
  private messageSub: Subscription; 

  constructor(private messageService: MessageService) { 
    this.messageInfo = new MessageInfo('11', '11');
    this.messageSub = this.messageService.getMessage().subscribe( (messageInfo) => ( this.messageInfo = messageInfo ));
  }

  ngOnInit() {
  }

  ngOnDestroy(){
    this.messageSub.unsubscribe();
  }

}
